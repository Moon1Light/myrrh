package net.bridge;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class Config {

    private final Properties props;

    public Config(File file) throws Exception {
        props = new Properties();
        props.load(new FileReader(file));
    }

    public String get(String key) {
        return props.getProperty(key);
    }

}
