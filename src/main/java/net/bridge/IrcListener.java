package net.bridge;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.SendMessage;
import net.engio.mbassy.listener.Handler;
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent;

public record IrcListener(Config cfg, TelegramBot tgBot, String tgChannel, String ircChannel) {

    @Handler
    private void onMessage(ChannelMessageEvent event) {
        if (event.getChannel().getName().equals(ircChannel)) {
            var msg = event.getMessage().replaceAll("\u0003\\d+", "");
            tgBot.execute(new SendMessage("@" + tgChannel, event.getActor().getNick() + ": " + msg));
        }
    }

}
