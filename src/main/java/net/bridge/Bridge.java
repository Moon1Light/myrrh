package net.bridge;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Update;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kitteh.irc.client.library.Client;

public class Bridge {

    private final Logger log = LogManager.getLogger(Bridge.class.getSimpleName());
    private final TelegramBot tgBot;
    private final Client ircBot;

    public Bridge(Config cfg, boolean skipOldMessages) {
        tgBot = new TelegramBot(cfg.get("telegram.token"));
        String tgChannel = cfg.get("telegram.channel");
        String ircChannel = cfg.get("irc.channel");

        System.out.println(tgChannel + " " + ircChannel);

        ircBot = Client.builder()
                .name(cfg.get("irc.name"))
                .nick(cfg.get("irc.name"))
                .realName(cfg.get("irc.name"))
                .server().host(cfg.get("irc.host")).then()
                .build();

        ircBot.addChannel(ircChannel);

        ircBot.getEventManager()
                .registerEventListener(new IrcListener(cfg, tgBot, tgChannel, ircChannel));

        tgBot.setUpdatesListener(updates -> {
            for (Update update : updates) {
                if (update.message() != null) {
                    var msg = update.message();

                    if (skipOldMessages && (msg.date() + 15) < System.currentTimeMillis() / 1000L) {
                        log.debug("skip telegram message");
                        continue;
                    }

                    if (msg.chat().username().equals(tgChannel)) {
                        if (msg.text() != null && !msg.text().isEmpty()) {
                            ircBot.sendMessage(ircChannel, msg.from().username() + ": " + msg.text());
                        }
                    }
                }
            }

            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });

        log.info("connecting to irc");
        ircBot.connect();
    }

}
